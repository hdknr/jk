.. Experimentals documentation master file, created by
   sphinx-quickstart on Thu Jan 19 17:24:40 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OAuth/Connect 情報連携基盤
=========================================

情報連携基盤:

.. toctree::
    :maxdepth: 2
    :numbered: 

    authority 
    flows
    identifiers

その他:

.. toctree::
    :maxdepth: 2

    connect_credential_flow

メモなど:

.. toctree::
    :maxdepth: 2

    idcon

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

