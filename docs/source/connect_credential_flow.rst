=========================================================================================
OpenID Connect Client Credentials Flow with JSON Web Token(JWT) Bearer Token 
=========================================================================================


Abstract
=============

This documentation will define Client Credential flow
for OpenID Connect authorization grant process with JWT Bearer Token,
and follows basic principle in Web Token (JWT) Bearer Token Profiles for OAuth 2.0 [:term:`JWT.BEARER`].

.. contents:: Table of Contents

Introduction
======================================

OpenID Connect Standard [:term:`CONNECT.STANDARD`] defines only 2 authorization grants flows:
**Authorization Code flow** and **Implicit flow**.

This documentation will define additional Client Credential flow for entities to handle 
token request with OpenID Conenct Standard binding.

With using "Web Token (JWT) Bearer Token Profiles for OAuth 2.0" [:term:`JWT.BEARER`],
a client directly request access token at an Authorization Server's Token Endpoint
and can optionally present OpenID Connect Request Object or Request File.

Flow
======================================

.. seqdiag:: diag/connect_credential_flow.diag


Token Endpoint
======================================

As like OAuth Creent Credentials flow,
Client directly request an access token at Token Endpoint.

Request
---------------------------------------------------------------

Connect Client Credentials flows requires the following parameters.

Parameters
^^^^^^^^^^^^^^

.. _4.4.2. Access Token Request: http://tools.ietf.org/html/draft-ietf-oauth-v2-23#section-4.4.2

.. glossary::

    grant_type
        - MUST be "**client_credentials**"
        - See "`4.4.2. Access Token Request`_ " for OAuth Client Credentials flow

    scope

        - As like defined in OAuth.  [#]_
        - See "`4.4.2. Access Token Request`_ " 

    client_assertion_type

        - follow the  [:term:`JWT.BEARER`]
        - MUST be "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"

    client_assertion

        - follows the process defined in  [:term:`JWT.BEARER`]

.. [#] **openid** should be specifed like the other Connect specs ?

.. todo::
    -  If client can use this flow, it MUST be authenticated with :term:`client_assertion` for  **code** flow as well.
    -  Check `Section 2.1 <http://openid.net/specs/openid-connect-registration-1_0.html#anchor3>`_ of [:term:`CONNECT.REGISTRATION`]. **token_endpoint_auth_type** seems to be a SINGLE value (= **client_secret_jwt**).
    - :term:`client_assertion` must contains access grants or acess control rules to UserInfo Endpoint, and to other endpoints specified by Distributed Claims in UserInfo JSON as well.

Request Object or Request File Paramters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In additional to parameters defined above,  
one of the following parameters can be optinally used to specify Connect Request Object or Request File.

.. glossary::

    request
        See "2.3.1.2. Request Parameter Method of Connect" in [:term:`CONNECT.STANDARD`] .

    request_uri
        See "2.3.1.3. Request File Method" in [:term:`CONNECT.STANDARD`] .

Response
----------------

Returning successfull responses MUST be followed the manner defined in 
"`3.1.2 Access Token Response <http://openid.bitbucket.org/openid-connect-standard-1_0.html#anchor11>`_"
in [:term:`CONNECT.STANDARD`] to return access tokens to UserInfo Endpoint.

Reutning erroh responses MUST be followed the manner defined in 
"`3.1.3 Access Token Error Response <http://openid.bitbucket.org/openid-connect-standard-1_0.html#anchor12>`_" 
in [:term:`CONNECT.STANDARD`] .


.. todo::
    - But, Client Credentials flow in OAuth returns ONLY an access token.
    - If we follow the OAuth, **id_token** and **reflesh** token should NOT be returned in Connect Client Credentials flow ?
    - I think, other tokens may be returned in Connect. Client must be authenticated at Token Endpoint anyway. Just too many grants.

Normative References
=====================

.. glossary::

    OAUTH
            Hammer-Lahav, E., Ed., Recordon, D., and D. Hardt, 
            “`The OAuth 2.0 Authorization Protocol 
            <http://tools.ietf.org/html/draft-ietf-oauth-v2-22>`_” , 
            ID draft-ietf-oauth-v2-22 (work in progress), September 2011.

    OAUTH.ASSERTION
            Mortimore, C., Ed., Campbell, B., Jones, M., and Y. Goland, 
            “`OAuth 2.0 Assertion Profile
            <http://tools.ietf.org/html/draft-ietf-oauth-assertions-01>`_”, 
            ID draft-ietf-oauth-assertions-01 (work in progress), October 2011.

    JWT.BEARER
            M. Jones (Microsoft), B. Campbell(Ping Identity Corp.),C. Mortimore(Salesforce.com),
            "`JSON Web Token (JWT) Bearer Token Profiles for OAuth 2.0 
            <http://self-issued.info/docs/draft-jones-oauth-jwt-bearer-02.txt>`_ " , 
            ID draft-jones-oauth-jwt-bearer-02 (work in prograss), November 14, 2011.     

    CONNECT.STANDARD
            N. Sakimura(NRI), J. Bradley(Independent), B. de Medeiros(Google), M. Jones(Microsoft), E. Jay(Illumila),
            "`OpenID Connect Standard 1.0 - draft 07
            <http://openid.bitbucket.org/openid-connect-standard-1_0.html>`_ ",
            December 22, 2011

    CONNECT.REGISTRATION
            Sakimura, N., Bradley, J., and M. Jones, 
            “`OpenID Connect Dynamic Client Registration 1.0 - draft 09
            <http://openid.net/specs/openid-connect-registration-1_0.html>`_",
            "
            January 2012.
