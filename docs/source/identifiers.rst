=============================
OAuth/Connect マイナンバー
=============================

.. todo::

    - 定義と関係性は？

        - マイナンバー
        - リンクコード
        - 符号
        - PPID

マイナンバー x 符号
=============================

.. list-table:: 

    *   -    
        -   マイナンバー
        -   符号

    *   -   利用者
        -   ユーザ−
        -   システム  

    *   -   番号体系
        -   :ref:`数字11桁 <identifiers_mynumber>`
        -   :ref:`任意の文字列 <identifiers_ppid>`

.. _identifiers_mynumber:

マイナンバー発番
=================

- 10桁のランダムユニーク数字列 + 1チェックデジット
- チェックデジットはmod 10/11 とか？

    - クレジットカードや ISBN レベルのチェックデジット

- Luhn mod 10
    - http://technohidelic.posterous.com/luhn-algorithm-wikipedia-the-free-encyclopedi

.. _identifiers_ppid:

符号発番
=========

- 「可逆性」コード : Authz Serverがマスターデータベースに問い合せしなくても、ユーザ−の canonical id (local id)が分かるようになっている
-  canonical id を含んだ何らかの文字列に、Resource Server に対するペアワイズ情報を入れた文字列を Authz Serverのプライベートキーで暗号化するとか。

    - "`Connect Message 2.1.1.1.1 <http://openid.bitbucket.org/openid-connect-messages-1_0.html#idtype>`_  (Draft 7, Dec 23,2011 ) ではAES128の例

-  サイズ制限は?

    - Connect 的には <= 255 byte 
    - 大きすぎると、 :ref:`PPIDセットを大量に送るとき <flows.additional_endpoint>`  などのオーバーヘッドなどに影響

Python サンプル
-------------------

.. code-block:: python

    import os
    import binascii
    from Crypto.Cipher import AES 
    
    PAD= '*' 
    
    def  ppid_from_id(id,key):
        iv= os.urandom(16)
        return binascii.hexlify( 
                AES.new(key, AES.MODE_CBC,iv).encrypt( id + PAD * ( 16 -  len(id) % 16 ) ) 
        ) + "-"+ binascii.hexlify(iv)
    
    def id_from_ppid(ppid,key):
        (cipkey,iv) = ppid.split('-')
        return AES.new(key, AES.MODE_CBC,binascii.unhexlify(iv)
               ).decrypt( binascii.unhexlify( cipkey ) ).replace(PAD,'')


テスト実行してみる。

.. code-block:: python

    class PPIDTEST(unittest.TestCase):
        def test_simple(self):
            import uuid
            key = uuid.uuid1().hex
            myid = '01234567890123456789'
    
            ppid = ppid_from_id(myid,key)
            id = id_from_ppid(ppid,key) 
    
            print ppid
            print id
            self.assertEqual(id,myid)

::

    (openid)hdknr@sqg:~$ nosetests -s 

    3fe8b2eafec9bcf6a97d158fd3db961a7e6068e5a0169059c9ab09a537219604-d5397daa3ac48e8817c39f1b741d26cc
    01234567890123456789

    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.228s

    OK
