# -*- coding: utf-8 -*-
import sys
if sys.version_info < (2,7):
    import unittest2 as unittest
else:
    import unittest

# ------ Begin  ------
import os
import binascii
from Crypto.Cipher import AES

PAD= '*'

def  ppid_from_id(id,key):
    iv= os.urandom(16)
    return binascii.hexlify( 
            AES.new(key, AES.MODE_CBC,iv).encrypt( id + PAD * ( 16 -  len(id) % 16 ) )
    ) + "-"+ binascii.hexlify(iv)

def id_from_ppid(ppid,key):
    (cipkey,iv) = ppid.split('-')
    return AES.new(key, AES.MODE_CBC,binascii.unhexlify(iv)
           ).decrypt( binascii.unhexlify( cipkey ) ).replace(PAD,'')

# ------ End ------

class PPIDTEST(unittest.TestCase):
    def test_simple(self):
        import uuid
        key = uuid.uuid1().hex
        myid = '01234567890123456789'

        ppid = ppid_from_id(myid,key)
        
        id = id_from_ppid(ppid,key)

        print ppid
        print id
        self.assertEqual(id,myid )

'''
(openid)hdknr@sqg:~$ nosetests -s 
94869656b78fa70c947674f9fc1a4c1f7d72918bb55a1b679088d1e79621bd04
01234567890123456789
.
----------------------------------------------------------------------
Ran 1 test in 0.228s

OK
'''
